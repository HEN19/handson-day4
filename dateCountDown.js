function getLastDayOfMonth(year, month) {
    return new Date(year, month + 1, 0).getDate();
}

function getDates() {
    let dates = [];
    const today = new Date();
    const currentYear = today.getFullYear();
    const currentMonth = today.getMonth();
    const lastDayOfMonth = getLastDayOfMonth(currentYear, currentMonth);

    for (let i = today.getDate(); i <= lastDayOfMonth; i++) {
        let currentDate = new Date(currentYear, currentMonth, i, today.getHours(), today.getMinutes(), today.getSeconds());

        let formattedDate = {
            Day: currentDate.toLocaleDateString('en-US', { weekday: 'long' }),
            Date: currentDate.toLocaleDateString('en-UK'),
            Time: currentDate.toLocaleTimeString('en-US'),
            Timezone: currentDate.toLocaleTimeString('en-US', { timeZoneName: 'short' }).split(' ')[2],
            LocalTimezone: currentDate.toLocaleTimeString('en-US', { timeZoneName: 'long' })
        };

        dates.push(formattedDate);
    }

    return dates;
}

const dates = getDates();
console.table(dates);

// Log the dates in a visually appealing format
console.log('DETAILED:');
dates.forEach((date, index) => {
    console.log(`\nDate ${index + 1}:`);
    console.log('---------------------------');
    console.log(`   Day: ${date.Day}`);
    console.log(`   Date: ${date.Date}`);
    console.log(`   Time: ${date.Time}`);
    console.log(`   Timezone: ${date.Timezone}`);
    console.log(`   Local Timezone: ${date.LocalTimezone}`);
  });
  